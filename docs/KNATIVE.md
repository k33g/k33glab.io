# Knative

> WIP :construction:

## 🇬🇧 Knative, smoothly, on K3S

- [Knative, the tool that makes Kubernetes lovable](https://k33g.gitlab.io/articles/2020-05-02-KNATIVE-K3S-EN-01.html)

## 🇫🇷 Knative, en douceur, sur K3S

- [Knative, l'outil qui rend Kubernetes sympathique](https://k33g.gitlab.io/articles/2020-05-02-KNATIVE-K3S-FR-01.html)
