---
title: 🇫🇷 Borg Collective, mes jouets pour apprendre Knative
lang: fr-FR
date: "2020-05-30"
month: "2020-05"
classification: "Knative"
teaser: ""
---

# 🇫🇷 Borg Collective, mes jouets pour apprendre Knative

> Toute résistance est inutile

J'ai progressé dans mon apprentissage de Knative, et j'ai rapidement réalisé que j'avais besoin d'outils supplémentaires pour faciliter ma vie de développeur. Si je peux expérimenter vite, je peux le faire pour de nombreuses choses at accélérer mon apprentissage.

Tout d'abord, j'ai besoin d'un cluster Kubernetes sur lequel je puisse installer Knative facilement. Et je veux aussi pouvoir créer et détruire à volonté mon cluster plusieurs fois par jour (on sait tous que Kubernetes est fait pour ça 😉).

J'ai donc créé des "toy projects" pour m'aider dans mon apprentissage de Knative. Ils sont tous dans ce groupe GitLab : [https://gitlab.com/borg-collective](https://gitlab.com/borg-collective).

Ils sont actuellement utilisables (pas forcément complet, ni l'état de l'art), et ils sont:

- **Unimatrix 0** : tout ce qu'il vous faut pour créer un cluster K3S et installer Knative dessus
- **7of9** : un runtime à base de container pour déployer rapidement une sorte de fonction sur la plateforme **Knative Serving**, sans passer par l'étaple de construction de l'image, et du coup faciliter mon processus de découverte. J'ai basé 7of9 sur  **GraalVM + Vert.x + Kotlin** et j'utilise les langages inclus dans GraalVM (JavaScript, Python, Ruby).
- **Locutus.wip** : c'est aussi un runtime à base de container pour s'amuser avec **Knative Eventing**. Là aussi, je voulais me passer de la phase de construction de l'image à chaque fois que je modifiais du code. Locutus.wip est basé sur **NodeJS + Express + CloudEvents SDK**. Alors pourquoi ".wip"? En fait la spécification CloudEvents n'est pas encore complètement cuite, et le SDK JavaScript non plus. 
- Bientôt : **Borg.Queen**, comme 7of9 mais en utilisant le **Kotlin script engine** - "stay tuned".

## Unimatrix 0

Le projet est ici : [https://gitlab.com/borg-collective/unimatrix-0](https://gitlab.com/borg-collective/unimatrix-0)

Donc, Unimatrix 0 est un ensemble de scripts pour créer un (mono) cluster K3S et installer dessus les composants de Knative. C'est extrêment simple à utiliser. J'utilise **Multipass** (je les exécutent sous OSX, mais théoriquement cela devrait fonctionner sous Linux et Windows - quelques tests à venir...) :

```bash
git clone git@gitlab.com:borg-collective/unimatrix-0.git
cd unimatrix-0
./create-vm.sh
./knative-serving.sh
./knative-dns.sh
./knative-eventing.sh
./enable-broker.sh
./knative-monitoring.sh
```

Et c'est tout! (vous pouvez modifier les spécifications de la vm en éditabt le fichier `vm.config` avant de lancer la création de la vm).

Le script va générer un fichier `k3s.yml` dans le répertoire `/config qui vous permettra de vous connecter à votre cluster "de l'extérieur" avec `kubectl` :

```bash
export KUBECONFIG=$PWD/config/k3s.yaml
kubectl get pods --all-namespaces
# or even run K9S
k9s --all-namespaces 
```

## 7of9 pour jouer avec Knative Serving

Maintenant que nous avons une plateforme Knative qui tourne, nous pouvons déployer un service dessus. Tout d'abord, vous aurez besoin de `kubectl` et de la CLI Knative `kn` (voir [Installing the Knative CLI](https://knative.dev/docs/install/install-kn/)), une fois installés, c'est très facile :

Créez u fichier `hello.js`:

```javascript
function hello(params) {
  return {
    message: "Hello World",
    total: 42,
    author: "@k33g_org",
    params: params.getString("name")
  }
}
```

Et ensuite créez le service :

```bash
kubectl create namespace k-apps

# create or update the service
kn service create --force hello-service \
--namespace k-apps \
--env FUNCTION_NAME="hello" \
--env LANG="js" \
--env FUNCTION_CODE="$(cat ./hello.js)" \
--env CONTENT_TYPE="application/json;charset=UTF-8" \
--image registry.gitlab.com/borg-collective/7of9:latest
```

Attendez un petit moment, et vous devriez obtenir une URL pour votre service qui ressemblera à celle ci :

```bash
http://hello-service.k-apps.192.168.64.70.xip.io
```
> `192.168.64.70` est l'IP de mon cluster local

```bash
curl -d '{"name":"Bob"}' \
-H "Content-Type: application/json" \
-X POST http://hello-service.k-apps.192.168.64.70.xip.io
```

Facile ! Non ?

## Locutus.wip pour jouer avec Knative Eventing

Alors je fais mes 1er pas avec Eventing, et je trouve que c'est plus compliqué à comprendre que la partie Serving (certainement parce la plupart du temps, je code des webapps). Du coup, pour faciliter mes expérimentations, j'ai créé Locutus.wip, et je trouve que c'est facile à utiliser.

Voici un exemple très simple, (revenez de temps en temps, j'ai prévu d'en écrire des plus complexes) :

Premièrement, nous devons créer un service et un bout de code qui va gérer l'évènement reçu (`params`) et l'évènement créé pour la réponse (`event`) :

```bash
read -d '' CODE << EOF
let hello = (event, params) => {

  console.log("🦊", params)

  return event
    .type('dev.knative.demo')
    .source('[Locutus]')
    .time(new Date())
}
EOF

kn service create --force locutus-wip-1 \
  --env FUNCTION_NAME="hello" \
  --env FUNCTION_CODE="$CODE" \
  --image registry.gitlab.com/borg-collective/locutus.wip:latest 
```

> le broker est injecté sur le namespace `default` pendant l'installation de Knative avec le projet Unimatrix 0 avec cette commande : `kubectl label namespace default knative-eventing-injection=enabled`

Ensuite, il faut créer une "source" (l'émetteur d'évènement), pour cela créez un fichier `source.yml` :

```yaml
apiVersion: sources.knative.dev/v1alpha2
kind: PingSource
metadata:
  name: test-ping-source
spec:
  schedule: "* * * * *"
  jsonData: '{"message": "Hello world 😃 "}'
  sink:
    ref:
      apiVersion: serving.knative.dev/v1
      kind: Service
      name: locutus-wip-1
```

Puis publiez la source :

```bash
kubectl apply -f source.yml
```

Et maintenant "checkez" régulièrement vote service (ce n'est pas instantané) :

```bash
kubectl logs -l serving.knative.dev/service=locutus-wip-1 -c user-container --since=1m
```

Au bout d'un moment, vous devriez obtenir quelque chose comme ceci :

```bash
🖐️ received data: { message: 'Hello world 😃 ' }
🦊 { message: 'Hello world 😃 ' }
👋 reply cloud event: {
  "specversion": "1.0",
  "id": "f38a6135-5e71-4e1b-a46d-9aa905ee83f3",
  "type": "dev.knative.demo",
  "source": "[Locutus]",
  "time": "2020-05-30T09:00:00.031Z"
}
```

Voilà, c'est tout pour le moment. J4ai besoin de préparer un talk à propos de tout ça, donc la documentation des projets va évoluer.

Je vous souhaite une très belle journée.

<disqus/>

<last-articles/>

