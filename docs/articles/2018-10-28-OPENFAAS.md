---
title: 🇬🇧 Use Express with OpenFaaS
lang: us-EN
date: "2018-10-28"
month: "2018-10"
classification: "serverless"
teaser: ""
---

# 🇬🇧 Use Express with OpenFaaS

Before moving on to a longer blog post (soon) on how to use OpenFaaS with GitLab, here is a quick blog post about templates.

There is an interesting organization OpenFass related: [https://github.com/openfaas-incubator](https://github.com/openfaas-incubator), where you can find a template based on **Express** : [https://github.com/openfaas-incubator/node10-express-template](https://github.com/openfaas-incubator/node10-express-template).

> "This template provides additional context and control over the HTTP response from your function."

This template is very easy to use:

## Create a new function using the Express template

First create a directory for your project:

```shell
mkdir express-project
cd express-project
```

Then, fetch the **Express** template:

```shell
faas template pull https://github.com/openfaas-incubator/node10-express-template
```

And finally, create the function:

```shell
faas new hello --lang node10-express
```

If you're using the same setup as described in the previous [blog post](https://k33g.gitlab.io/articles/2018-10-20-OPENFAAS.html), you have to change the `hello.yml` file like that:

```yaml
provider:
  name: faas
  gateway: http://openfaas.test:8080
functions:
  hello:
    lang: node10-express
    handler: ./hello
    image: registry.test:5000/hello:latest
```

## Create your function thanks the help of Express

At the beginning of the blog post, I quoted the documentation of the template: *"This template provides additional context and control over the HTTP response from your function."*. So, update the `handler.js` file like that:

```javascript
"use strict"

module.exports = (event, context) => {

  switch(event.path) {
    case "/about":
      context
        .headers({"Content-Type": "text/html"})
        .status(200)
        .succeed("<h1>Hello function with Express template</h1>")
      break;
    case "/sum":
      try {
        context
          .headers({"Content-Type": "application/json"})
          .status(200)
          .succeed({
            result: event.query.numbers.split(",")
              .map(item => Number(item))
              .reduce((acc, number) => number + acc)
          })
      } catch (error) {
        context
          .headers({"Content-Type": "application/json"})
          .status(500)
          .fail({
            message:"😡 Bad query: write something like that: hello/sum?numbers=10,20,12"
          })
      }
    break;
      default:
        context
          .headers({"Content-Type": "text/plain"})
          .status(200)
          .succeed("👋 Hello 🌍 World 😀")
  }
}
```

What will it do?

- If you invoke the function like that: `echo | faas-cli invoke hello/about` you'll get this response: `<h1>Hello function with Express template</h1>`
![html](./images/openfaas-html.png)
- If you invoke the function like that: `echo | faas-cli invoke hello/sum?numbers=30,12` you'll get this response: `{"result": 42}`
![json](./images/openfaas-json.png)
- If you invoke the function like that: `echo | faas-cli invoke hello/sum?something=30,12` you'll get this response: `{"message": "😡 Bad query: write something like that: hello/sum?numbers=10,20,12"}`
- Otherwise, you'll get `""👋 Hello 🌍 World 😀"`
![text](./images/openfaas-text.png)

## Build an deploy the function

It's easy:

```shell
# Build and push to the Docker registry
faas-cli build -f hello.yml
faas-cli push -f hello.yml 

# Deploy to OpenFaaS
#   - Set the url of the OpenFaaS server
#   - Authenticate
#   - Deploy
export OPENFAAS_URL=http://openfaas.test:8080
echo -n 21d0b92900a5cd99296cd4f8d96b260ec687fd6464a1eb522b6e699ee7896eed | faas-cli login --username=admin --password-stdin

faas-cli deploy -f hello.yml
```

And now, you can use your new function:

```shell
echo | faas-cli invoke hello/sum?numbers=30,12
```

That's all for today 😃 It's possible to do these kind of things with Vert-X and Java (or Kotlin), even with Scala (a lot of things to digg).

Have a good Sunday 👋

<disqus/>

<last-articles/>