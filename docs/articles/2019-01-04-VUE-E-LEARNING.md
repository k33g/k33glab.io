---
title: 🇫🇷 Initiation aux Vue components avec GitLab 🦊
lang: fr-FR
date: "2019-01-04"
month: "2019-01"
classification: "javascript, vue.js"
teaser: ""
---

# 🇫🇷 Initiation aux Vue components avec GitLab 🦊

Mais quel titre bizarre pour mon 1er blog post de l'année (au fait bonne année à tous). J'ai découvert pendant mes vacances qu'il était assez simple de fournir des supports de cours à partir d'un projet GitLab (mon inspiration vient d'une certification interne peoposée par GitLab, je n'ai rien inventé).

Du coup, j'ai créé un **"mini cours"** d'initiation sur les **vue components** en mode e-learning via GitLab.

Je vais sonc vous présenter: 

- rapidement le principe de construction
- et enfin comment utiliser le support

## Principe de construction du support de cours

Le principe est très simple.

Vous créez des labels pour les différentes parties du cours:

![text](./images/elearning-labels.png)

Ensuite vous créez chaque partie de la leçon dans une issue (et vous affectez le bon label)

![text](./images/elearning-issues.png)

👋 **Important**: Créez une **Milestone** et affectez les issues à cette milestone. Cela permettra à vos apprenants de suivre leur progression:

![text](./images/elearning-milestone.png)

Vous pouvez aussi introduire votre cours, avec le plan et un lien vers la 1ère issue pour démarrer:

![text](./images/elearning-plan.png)

Ce n'est pas bien plus compliqué que ça, ensuite à vous d'être imaginatifs (par exemple: mettre un lien dans l'issue N vers l'issue N+1, etc...)

Ensuite pour pouvoir distribuer votre cours, utilisez la [fonction export de GitLab (puis import)](https://docs.gitlab.com/ee/user/project/settings/import_export.html)

## Donc installer le support de cours

Pour cela, 
- il vous faut un compte sur [GitLab.com](https://www.gitlab.com) ou avoir votre propre instance de GitLab
- ensuite, téléchargez mon "mini-cours" ici: 👋 **[2019-01-04_22-07-525_bots-garden_training-materials_export.tar.gz](/files/2019-01-04_22-07-525_bots-garden_training-materials_export.tar.gz)**
- puis importez le sur [GitLab.com](https://www.gitlab.com) ou sur votre propre instance:

![text](./images/elearning-export.png)

C'est tout, ensuite il ne vous reste plus qu'à suivre les instructions.

**N'hésitez pas à me faire vos remarques** 😉

<disqus/>

<last-articles/>