---
title: 🇬🇧 Every GitLab Page deserves a real CI/CD
lang: en-EN
date: "2020-07-23"
month: "2020-07"
classification: "GitLab CI"
teaser: ""
---

# 🇬🇧 Every GitLab Page deserves a real CI/CD

> In this blog post I will explain how to structure a GitLab Page, but more powerful how to do a review application like with any other web application project. **(I used free plan of GitLab.com)**

Firstavall, what are GitLab Pages? You can have a look to the GitLab documentation here: https://docs.gitlab.com/ee/user/project/pages/ but the simplest explanation is "With GitLab Pages, you can publish static websites directly from a repository in GitLab."

And GitLab Pages are a very good way to learn GitLab CI/CD 😃

## Create your first GitLab Page

It's very easy: 

- Create a repository on GitLab (I created mine in a group: [https://gitlab.com/my-little-pages/food-and-drinks](https://gitlab.com/my-little-pages/food-and-drinks))
- Add a `website` directory (you can choose another name) with you `index.html` page

You should have a project like that:


```bash
.
└── website
   └── index.html
```

- Add a `.gitlab-ci.yml` file to the project with this content:

```yaml
image: node:12.0-slim

stages:
  - 📝publish

pages:
  stage: 📝publish
  script:
    - mkdir -p public/{js,css}
    - cp ./website/index.html public
  artifacts:
    paths:
      - public
```

> - 🖐️ the `pages` job name is mandatory (it will trigger automatically the deployment of the pages)
> - 🖐️ the artifact section is mandatory
> - and your script will just create the `public` directory and copy all files to the `public` directory
> - 🖐️ the name `public` is mandatory
> - it's not mandatory to use `image: node:12.0-slim`, but the Docker image is smaller
> - emojis are not mandatory

- Commit your source code, then the `pages` job will be triggered and followed by a `pages:deploy` job.
- Wait a moment, and a few seconds later you should reach your pages at this url: [https://<YOUR_GROUP_NAME>.gitlab.io/<YOUR_PROJECT_NAME>](https://<YOUR_GROUP_NAME>.gitlab.io/<YOUR_PROJECT_NAME>) (in my case, if you want to see the result, it's: [https://my-little-pages.gitlab.io/food-and-drinks](https://my-little-pages.gitlab.io/food-and-drinks))

🎉 It's very simple, but let's doing more serious thinks 😉

## Add environment and publish only on the master branch (or whatever the name you gave to your main branch)

Update your `.gitlab-ci.yml` file like this:

```yaml
image: node:12.0-slim

stages:
  - 📝publish

pages:
  stage: 📝publish
  script:
    - mkdir -p public/{js,css}
    - cp ./website/index.html public
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
  environment:
    name: production/${CI_PROJECT_NAME}/${CI_COMMIT_REF_NAME}
    url: https://${CI_PROJECT_NAMESPACE}.gitlab.io/${CI_PROJECT_NAME}
```

- Commit your source code **on master**, then the page will be deployed again. If you commit on a feature branch, the pipeline will not be triggered thanks to the rule `if: $CI_COMMIT_BRANCH == "master"`
- Go to the **Environment menu** of your project and you'll see that the job has created a new line with a new environment, and there is even a button to reach your webpage.

![gl-pages-01.png](./pictures/gl-pages-01.png)

But, we can do more 🙂

## Review application with GitLab Pages

One of my top five favorite GitLab features is [Review Apps](https://docs.gitlab.com/ee/ci/review_apps/): *"Review Apps is a collaboration tool that takes the hard work out of providing an environment to showcase product changes."*.

So, it means that you can deploy a GitLab Page from a feature branch, without impacting your production page, in a new temporary environment 🤩

It's not documented on how to use this feature with the GitLab Pages. But the trick is easy. Let's me explain:

Change again your `.gitlab-ci.yml` file like this by adding 2 new jobs:

```yaml
🤔pages:preview:
  stage: 📝publish
  script:
    - mkdir -p public/{js,css}
    - cp ./website/index.html public
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_MERGE_REQUEST_IID
  environment:
    name: preview/${CI_PROJECT_NAME}/${CI_COMMIT_REF_NAME}
    url: https://${CI_PROJECT_NAMESPACE}.gitlab.io/-/gl-pages/${CI_PROJECT_NAME}/-/jobs/${CI_JOB_ID}/artifacts/public/index.html
    on_stop: 😢pages:preview:stop

😢pages:preview:stop:
  stage: 📝publish
  rules:
    - if: $CI_MERGE_REQUEST_IID
      when: manual
  allow_failure: true
  environment:
    name: preview/${CI_PROJECT_NAME}/${CI_COMMIT_REF_NAME}
    action: stop
  script:
    - echo "👋 bye"
```

- Commit your source code by creating a **Merge Request**, then the "preview" page will be deployed "from the new feature branch" thanks to the new rule `if: $CI_MERGE_REQUEST_IID`
- In the first job you can read: `on_stop: 😢pages:preview:stop` that means that the review environment will be dropped when you'll merge the MR and delete the feature branch by calling the second job.
- You can note that the name of the environment is different: `name: preview/${CI_PROJECT_NAME}/${CI_COMMIT_REF_NAME}`
- And the trick is about the way of building the url: `url: https://${CI_PROJECT_NAMESPACE}.gitlab.io/-/gl-pages/${CI_PROJECT_NAME}/-/jobs/${CI_JOB_ID}/artifacts/public/index.html`

Then now, you can discover a new environment that allows you to reach the review page with the new url:

![gl-pages-02.png](./pictures/gl-pages-02.png)

And the very nice thing is that you can access to the review application from your **Merge Request**:

![gl-pages-03.png](./pictures/gl-pages-03.png)

Now if you merge your updates (and delete the feature branch), the review environment will be deleted. (you can do it manually too), and your updates will be deployed to the "production" environment.

It's very simple. Now you have a real workflow and a real CI/CD process for your pages. Next time, I will explain how to run unit tests and linter, and provide feedbacks in the **Merge Request**, and again with the GitLab Pages.

Happy experiments 👋
<disqus/>

<last-articles/>

