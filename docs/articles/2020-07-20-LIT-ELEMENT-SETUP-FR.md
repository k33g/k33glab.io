---
title: 🇫🇷 Lit-Element, commencer doucement
lang: fr-FR
date: "2020-07-20"
month: "2020-07"
classification: "WebComponent"
teaser: ""
---

# 🇫🇷 Lit-Element, commencer doucement

## C'était le bon temps

Il y a quelques années, "faire" du JavaScript était simple. Il suffisait d'inclure vos librairies JavaScript dans votre page html, et vous étiez prêts à coder. Un simple refresh de votre navigateur suffisait à vérifier l'avancée de vos travaux.

> c'était le bon temps ...
```html
  <script src="libs/vendors/jquery.js"></script><!--V 1.10.2 -->
  <script src="libs/vendors/underscore.js"></script><!--V 1.5.2 -->
  <script src="libs/vendors/backbone.js"></script><!--V 1.1.0 -->
```

Cette facilité permettait de tester facilement un nouveau framework (je download, j'inclus, et hop !), et a permis à beaucoup d'entre nous de se "mettre à JavaScript" (voire même à la programmation). Bref, c'était le bon temps.

Puis quelqu'un eut l'idée de faire du tooling pour les projets JavaScript (probablement un fou complètement jaloux des builds Maven de nos amis "Javaistes"), et le ticket d'entrée pour développer en JavaScript a commencé à être de plus en plus cher. Aujourd'hui, quand vous n'avez pas l'habitude, avant de pouvoir afficher **"👋 hello world 🌍"** dans votre navigateur, va vous demander plusieurs heures **(1)** de mise en place d'outils qui vont vous permettre de "builder" et servir votre **"toute petite page html"**.

> **(1)** plusieurs heures 🙀 !!! Oui ! J'insiste ! Si vous n'êtes pas développeur JavaScript, vous allez devoir passer par quelques étapes avant de pouvoir commencer à coder (installer node js, installer n frameworks de lint, build, generation css, ...).

... Et le plus souvent je m'arrête avant la fin et me dis que finalement le dev BackEnd c'est très bien.

Bref, **développer en JavaScript** n'est plus aussi simple qu'avant et demande beaucoup plus d'effort au démarrage. Ce qui est fort dommage, car le dimanche matin quand je décide de geeker un peu, j'ai envie d'être dans le code tout de suite (parce qu'en plus, ma compagne va probablement me demander d'aller faire le marché, donc mon temps est limité).

Bon, je m'égare. Le propos de cet article est de vous dire que **"tout n'est pas perdu"**, on peut faire comme avant, même avec des frameworks **"nouveaux"**, et aujourd'hui je vais utiliser **[Lit-Element](https://lit-element.polymer-project.org/)** un framework très sympa, très simple à base de web components et qui normalement demande d'utiliser du tooling. Mais en fait, on peut faire autement.

Il faut juste un peu de travail de préparation, mais ça je l'ai fait pour vous, donc vous n'aurez rien à faire 🎉. Je vous détaille donc tout ça aujourd'hui.

## Préparation de votre webapp (à ne faire qu'une seule fois)

> 🖐️ mes exemples sont faits sous OSX, fonctionne sous Linux. Pour Windows cela s'adapte facilement, sinon, utilisez Git Bash.

### Pré-requis

Il vous faut **[SnowPack](https://www.snowpack.dev/)**

```bash
npm install -g snowpack
```

### Génération de la structure de la webapp

Mettons que je veuille créer une webapp **"Hello World"**

```bash
mkdir hello-world
cd hello-world
mkdir public
mkdir public/{js,css,components}
cd public
npm init --yes
npm install --save lit-element
npm install --save pwa-helpers # 1️⃣
```

> 1️⃣ cette ligne n'est pas obligatoire, mais pwa-helper fournit un routeur bien pratique

Vous devriez obtenir cette arborescence:

```
.
└── public
   ├── components
   ├── css
   ├── js
   ├── node_modules
   ├── package-lock.json
   └── package.json
```

Editez le fichier `package.json` pour lui ajouter ceci:

```json
"snowpack": {
  "install": [
    "lit-element",
    "pwa-helpers"
  ]
}
```

Donc le contenu de votre fichier `package.json` devrait correspondre à ceci:


```json
{
  "name": "public",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "dependencies": {
    "lit-element": "^2.3.1",
    "pwa-helpers": "^0.9.1"
  },
  "snowpack": {
    "install": [
      "lit-element",
      "pwa-helpers"
    ]
  }
}
```

> 🖐️ sauvegardez

### Génération des librairies JavaScript "embarquées"

```bash
cd hello-world/public 
snowpack
rm -rf node_modules # 1️⃣
```

1️⃣ Nous n'avons plus besoin de `node_modules`, SnowPack a "extrait et construit" tout ce dont nous avons besoin et a copié les dépendances dans le dossier `web_modules`

```bash
✔ snowpack install complete. [0.42s]

  ⦿ web_modules/       size       gzip       brotli   
    ├─ lit-element.js  115.08 KB  27.67 KB   23.4 KB    
    └─ pwa-helpers.js  11.34 KB   2.97 KB    2.42 KB   
```

Maintenant, nous devrions avoir l'arborescence suivante:

```bash
.
├── components
├── css
├── js
├── package-lock.json
├── package.json
└── web_modules
   ├── import-map.json
   ├── lit-element.js
   └── pwa-helpers.js
```

## Il est temps de faire des composants Lit-Element

Dans le dossier `components` ajouter les 2 composants suivants:

> `AppTitle.js`
```javascript
import { LitElement, html } from '../web_modules/lit-element.js'

export class AppTitle extends LitElement {
  render() {
    return html`<h1">🖖 live long and prosper 🌍</h1>`
  }
}

customElements.define('app-title', AppTitle)
```

> `MainApplication.js`
```javascript
import { LitElement, html } from '../web_modules/lit-element.js'
import {} from './AppTitle.js'

export class MainApplication extends LitElement {
  render() {
    return html`
      <div>
        <app-title></app-title>
      </div>
    `
  }
}

customElements.define('main-application', MainApplication)
```

## Créer une page `index.html`

à la racine du dossier `public` créez une page `index.html`:

> /public/index.html
```html
<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Hello World!</title>
	</head>
	<body>
    <main-application></main-application>
  </body>
  
	<script type="module">
    import {} from '/components/MainApplication.js'
  </script>
  
</html>  
```

Voilà maintenant, vous avez tout ce qu'il faut pour commencer vos premiers web components. Il vous suffit de **servir** votre page avec n'importe quel serveur http (j'utilise [https://www.npmjs.com/package/http-server](https://www.npmjs.com/package/http-server))

```bash
cd hello-world
http-server
```

Et vous n'avez plus qu'à naviguer vers [http://localhost:8080](http://localhost:8080) pour voir vos premiers web components en action (et cela fonctionne aussi sous FireFox).

## Et ...

Et pour vous éviter de bosser, le code est tout prêt à être utilisé ici: [https://gitlab.com/lit-element-softly/hello-world](https://gitlab.com/lit-element-softly/hello-world)

Très bonne semaine à tous 😃

<disqus/>

<last-articles/>

