---
title: 🇬🇧 How to use GitLab Container Registry on GitLab.com
lang: en-EN
date: "2020-04-05"
month: "2020-04"
classification: "gitlab"
teaser: ""
---

# 🇬🇧 How to use GitLab Container Registry on GitLab.com

My previous blog post explained [how to use a GitLab project as a npm registry](https://k33g.gitlab.io/articles/2020-04-01-GITLAB-NPM-REGISTRY.html). 

Today, I will explain, how to use a GitLab project as a **conatainer registry**:

- How to build a container image with GitLab CI
- How to host this container image (it's automatic 😉)
- How to use your new image with GitLab CI

Again, I used [Google CodeLabs Tools](https://github.com/googlecodelabs/tools) to generate the tutorial. 

This new tutorial is here:  **[How to use GitLab Container Registry on GitLab.com](https://tanuki-core-tutorials.gitlab.io/docker.registry)**

![alt codelab](./pictures/gitlab-docker-registry.png)

- The source code is here: [Container Registry CodeLab](https://gitlab.com/tanuki-core-tutorials/docker.registry)
- The sample of the tutorial is here: [Use a Markdown Linter in your CI](https://gitlab.com/tanuki-core-tutorials/markdown.linter)

That's all 🎉 and stay healthy 

<disqus/>

<last-articles/>

