---
title: 🇬🇧 How to use GitLab NPM Registry on GitLab.com
lang: en-EN
date: "2020-04-01"
month: "2020-04"
classification: "gitlab"
teaser: ""
---

# 🇬🇧 How to use GitLab NPM Registry on GitLab.com

2 days ago, GitLab announced "[18 GitLab features are moving to open source](https://about.gitlab.com/blog/2020/03/30/new-features-to-core/)". And some these features are my favorite features. Some of my followers know that I'm a JavaScript addict. So this new blog post is a tutorial on how to use GitLab NPM Registry on GitLab.com.

This time I will change the format. Some weeks ago I discovered [https://github.com/googlecodelabs/tools](https://github.com/googlecodelabs/tools), the tool to generate code labs like the Google CodeLabs, and I found in love with it. 

So, today this is my tutorial as a CodeLab: 👋 here:  **[How to use GitLab NPM Registry on GitLab.com](https://tanuki-core-tutorials.gitlab.io/npm.registry/#0)**

![alt codelab](./pictures/gitlab-npm-registry.png)

- The source code is here: [NPM Registry CodeLab](https://gitlab.com/tanuki-core-tutorials/npm.registry)
- The sample of the tutorial is here: [The Cyst project used for this tutorial](https://gitlab.com/tanuki-core-tutorials/cyst)

That's all 🎉 and stay healthy 

<disqus/>

<last-articles/>

