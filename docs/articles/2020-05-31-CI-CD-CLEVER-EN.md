---
title: 🇬🇧 Deploy quickly on Clever Cloud with GitLab CI 
lang: en-EN
date: "2020-05-31"
month: "2020-05"
classification: "GitLab CI"
teaser: ""
---

# 🇬🇧 Deploy quickly on Clever Cloud with GitLab CI 

> What is Clever Cloud? It's a French PaaS operated by developers for the developers (but not only the developers of course), and it's here [https://www.clever-cloud.com/](https://www.clever-cloud.com/)

The Clever Cloud team provides a CLI to deploy your web applications quickly on their PaaS, and there is a **Docker** version of this CLI 🎉, so today I offer you a very simple CI/CD snippet to boost your deployments on Clever Cloud (when you merge on the `master` branch):

Add this to the `.gitlab-ci.yml` file of your GitLab project

```yaml
image: clevercloud/clever-tools:latest

variables:
  CLEVER_KIND_APPLICATION: "node" # change the value if needed
#-------------------------------
# Deploy
#-------------------------------
production:deploy:to:clever-cloud:
  only:
    - master    
  environment:
    name: production-clever-cloud/${CI_PROJECT_NAME}
    url:  https://${CI_PROJECT_NAME}.cleverapps.io
  script: |
    CREATE_CC_APP=false
    clever link -o ${ORGANIZATION_ID} ${CI_PROJECT_NAME} || CREATE_CC_APP=true
    if ${CREATE_CC_APP}; then clever create --type ${CLEVER_KIND_APPLICATION} -o ${ORGANIZATION_ID} ${CI_PROJECT_NAME}; sleep 10; fi
    clever domain add ${CI_PROJECT_NAME}.cleverapps.io || true
    clever deploy -f

#-------------------------------
# Remove - this is a manual job
#-------------------------------
production:remove:from:clever-cloud:
  when: manual
  only:
    - master   
  environment:
    name: production-clever-cloud/${CI_PROJECT_NAME}
    action: stop
  script: |
    DELETE_CC_APP=true
    clever link -o ${ORGANIZATION_ID} ${CI_PROJECT_NAME} || DELETE_CC_APP=false
    if $DELETE_CC_APP; then clever delete -y -a ${CI_PROJECT_NAME}; fi
```

> Requirements: add theses environment variables `CLEVER_SECRET`, `CLEVER_TOKEN` and `ORGANIZATION_ID` to your CI/CD settings

Now, when you'll merge something on the `master` branch it will be deployed on Clever Cloud platform.

## 🖐️ if you used the GitLab shared runner on GitLab.com

replace:

```yaml
image: clevercloud/clever-tools:latest
```

by

```yaml
image: 
  name: clevercloud/clever-tools:latest
  entrypoint: ["/bin/sh", "-c"]
```

<disqus/>

<last-articles/>

