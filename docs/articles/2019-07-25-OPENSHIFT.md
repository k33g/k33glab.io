---
title: 🇬🇧 Install OpenShift with Vagrant
lang: us-US
date: "2019-07-25"
month: "2019-07"
classification: "linux, openshift"
teaser: ""
---

# 🇬🇧 Install OpenShift with Vagrant

I'm a developer, and I'm often not comfortable with LAN, system administration ... stuff, but I am usually stubborn. I'm struggling for several days (weeks?) with a specific configuration to prepare a future demonstration. And finally, I succeeded.
I'm not sure that I understood all that I did, but I can reproduce it, so I wrote all the steps.

But first, this is my configuration (or my requirements)

## Requirements

- I need an OpenShift server (a ssingle-node cluster), and I will install it on a NUC (OS: Linux) inside a Virtual Machine thanks to Vagrant.
- I need to connect to my OpenShift server from a Mac laptop.
- The NUC and the Mac computer are connected to the same local network (my home wifi box).

## Prerequisites on the NUC side

You need to install:
- VirtualBox
- Vagrant
> My NUC is under Ubuntu `18.04`

## OpenShift setup

This is my `Vagrantfile`: so, create a directory on your Linux server (in my case, it's on my NUC)

```ruby
OC_DOMAIN = "oc-chewie.test"
OC_IP = "172.16.245.100"
OC_PUB_IP = "192.168.1.100"
OC_NAME = "oc-chewie"

BOX_IMAGE = "bento/ubuntu-16.04"
VERSION = "1.0"

Vagrant.configure("2") do |config|
  config.vm.box = BOX_IMAGE

  ENV['LC_ALL']="en_US.UTF-8"

  config.vm.define "#{OC_NAME}" do |node|

    node.vm.hostname = "#{OC_NAME}"
    
    # perhaps you will need to adapt the bridge, it's relate to your material
    # usually on OSX, its `bridge: "en0: Wi-Fi (AirPort)"`
    
    node.vm.network "public_network", ip:"#{OC_PUB_IP}",  bridge: "wlp58s0"
    node.vm.network "private_network", ip: "#{OC_IP}"

    node.vm.network "forwarded_port", guest: 8443, host: 8443

    node.vm.provider "virtualbox" do |vb|
      vb.memory = 10240
      vb.cpus = 4
      vb.name = "#{OC_NAME}"
    end

    node.vm.provision :docker

    node.vm.provision :shell, inline: <<-SHELL
      apt-get update
      apt-get install -y curl

      # openshift cluster
      cd /tmp/
      curl -Lso oc-cli.tgz  https://github.com/openshift/origin/releases/download/v3.11.0/openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit.tar.gz
      tar -xvzf oc-cli.tgz
      cp openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit/oc /usr/local/bin/
      cp openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit/kubectl /usr/local/bin/
      chmod +x /usr/local/bin/oc
      chmod +x /usr/local/bin/kubectl

      # Optionally add what you want:
      echo "" >> /etc/hosts
      echo '#{OC_PUB_IP} #{OC_DOMAIN}' >> /etc/hosts
      echo "" >> /etc/hosts

      # Add unsecure registry (this is a prerequisite - see the official documentation)
      echo "" >> /etc/docker/daemon.json
      echo '{' >> /etc/docker/daemon.json
      echo '  "insecure-registries" : ["172.30.0.0/16"]' >> /etc/docker/daemon.json
      echo '}' >> /etc/docker/daemon.json
      echo "" >> /etc/docker/daemon.json

      service docker restart

    SHELL

    # start the cluster
    node.vm.provision "start", type: "shell" , run: "always" , inline: <<-SHELL
      oc cluster up --public-hostname=#{OC_PUB_IP}
    SHELL

  ends

end
```

And run this command `vagrant up`, take a ☕️ and wait some minutes.

## First application deployment (on the linux side)

If all is ok, you should be able to connect to the OpenShift server with this url: https://192.168.1.100:8443

After some warnings (about the fact that the connection isn't safe) you should get this:

![text](./images/oc-001.png)

You can connect with this credentials: 
- user:`system`
- password: `admin`

This is the "welcome" page:

![text](./images/oc-002.png)

Click on the `nodejs` icon to create a new webapplication

![text](./images/oc-003.png)

Click on the `Next >` button

![text](./images/oc-004.png)

- Then you can add your application to an existing project or create a new project
- Give a name to your application (in my case it's `hey`)
- Choose a Git repository (use the Git project sample: click on `Try Sample Repository`)
- And finally, click on the `Create` button

![text](./images/oc-005.png)

After the confirmation pop-up, click on the `Close` button

![text](./images/oc-006.png)

At, the top right corner, you can see the `node` project, so, select it:

![text](./images/oc-007.png)

You see that the `hey` application has been deployed, and the proposed url is [http://hey-node.192.168.1.100.nip.io/](http://hey-node.192.168.1.100.nip.io/), and now, the **most important thing** of this document is that:

**If your computer** refuses to resolve the `nip.io` domain:

- On OSX, go to the **Network Settings**
  - Choose your network
  - Click on the `Advanced...` button
  - Chose the DNS panel
  - add this DNS: `8.8.8.8` and click on `Ok`, then `Apply`
- On Linux,
  - Edit (as `root`) this file `/etc/resolv.conf`
  - add this line `nameserver 8.8.8.8`
  - save it 

Now you can acces to your webapp on [http://hey-node.192.168.1.100.nip.io/](http://hey-node.192.168.1.100.nip.io/):

![text](./images/oc-008.png)

🎉 next step (tomorrow?) deploy your own project from a GitLab repository (and with a GitLab runner)

👋 Have a nice day

<disqus/>

<last-articles/>