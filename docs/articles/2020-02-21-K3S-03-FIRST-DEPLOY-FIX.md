---
title: 🇫🇷 Kit de survie Kubernetes pour les développeurs avec K3S - Partie 2 - Fix
lang: fr-FR
date: "2020-02-21"
month: "2020-02"
classification: "kubernetes"
teaser: ""
---

# 🇫🇷 [🐞Fix] Kit de survie Kubernetes pour les développeurs avec K3S - Partie 2: Le déploiement

Ce matin dans la partie **1ère partie: "Service"**, je décrivais la partie **Service** du déploiement de cette façon:

```yaml
---
# Service
apiVersion: v1
kind: Service
metadata:
  name: amazing-web-app
spec:
  selector:
    app: amazing-web-app
  ports:
    - port: 80
      targetPort: 8080
  type: LoadBalancer
```

Sur les conseils de [Louis Tournayre](https://twitter.com/_louidji) j'ai enlevé la ligne `type: LoadBalancer`

## Explication rapide

J'utilise un Ingress, donc mon Service est plutôt de type **ClusterIP** (et pas LoadBalancer). Il y a plusieurs types de Services ([kubernetes-services](https://www.bmc.com/blogs/kubernetes-services/)) dont **ClusterIP** qui utilise une IP interne au sein du cluster et accessible avec un nom DNS.

## Attention

Si vous avez déjà déployé avec l'ancienne version, pour corriger c'est facile:

- Supprimez ce que vous avez déployé (en utilisant l'ancien fichier):
  ```bash
  kubectl delete -f ./kube/deploy.yaml
  ```
- Corrigez en supprimant `type: LoadBalancer`
- Sauvegardez
- Redéployez:
  ```bash
  kubectl apply -f ./kube/deploy.yaml
  ```

Et en plus vous allez voir disparaître les lignes rouges "parasites" toutes laides de la console **K9S**

![alt k9s](./pictures/k9s-fix-deploy.png)

<disqus/>

<last-articles/>

