---
title: 🇫🇷 La Bruschetta m'a sauvée
lang: fr-FR
date: "2020-04-16"
month: "2020-04"
classification: "cook"
teaser: ""
---

# 🇫🇷 La Bruschetta m'a sauvée

- Par avance, toutes mes excuses à mes amis Italiens pour les libertés que j'ai prises avec cette recette.
- Il n'y a pas de durée précise de cuisson, de température, ... tout se fait au nez, au doigt, à l'oeil et à la chance

## Tout a commencé ...

Tout a commencé autour du 15 jour de confinement, où ma compagne me demande d'aller acheter du pain. 

- "Mais bien sûr! Quel type de pain?" (*là c'est l'ancien chef de projet qui parle, c'est bien d'avoir des spécifications pour pas être ennuyé à la livraison*).
- "C'est toi qui voit, prend ce que tu veux" 🤔
- "Ok, c'est parti 🚀"

Donc direction la boulangerie du quartier, attente dehors pour respecter le "pas plus de 2 dans le magasin". Enfin, un pied à l'intérieur, et là un beau pain aux graines (de bien 2kgs ... peut-être plus) qui me regarde avec les yeux du Chat Potté. Emballé c'est pesé - Mission accomplie 🎉

C'est de retour à la maison que le drame a commencé:

- "Mais c'est quoi ça!? Qu'est ce que l'on va faire de tout ce pain?"
- 😮 "Euh, tu m'a dis, je cite *prend ce que tu veux*"
- 😡 "Mais ça ne t'empêche pas de réfléchir? si? t'a intérêt à le b... ton pain"
- 🤔💡 "Mais j'ai réfléchi! Le pain aux graines ça se concerve très bien, et je l'ai acheté pour faire des **bruschettas** en utilisant tout ce qui nous reste dans le frigo"
- 😠 "Mais bien sûr..."
- 😬 "Tout à fait, tu verras ce soir, ça va être une turie"

## Mes 1ères Bruschettas

Vous allez voir ce n'est pas compliqué (pour 4):

### Préparation

- j'ai découpé 6 tranches (== 1.5 tranche par personnes) *donc vous coupez en 2, 2 des tranches*
- je les ai mises au four (préchauffé avant autour de 200° ... mais c'était peut-être moins) sur une plaque recouverte de papier cuisson
- j'ai surveillé avant que ça grille (de temps en temps vous ouvrez la porte du four pour vérifier que le pain a commencé à durcir: un peu toasté)
- ensuite, je les retire du four pour:
  - les frotter avec de l'ail (normalement à la fin il ne doit plus y avoir d'ail)
  - ensuite verser "un peu" d'huile d'olive sur chacune des tartines

### Garniture

C'est là où vous devez improviser

Sur les 4 tranches entières j'ai dans l'ordre posé:

- de fine lamelles de tomate
- des oignons émincés
- des lamelles de jambon cru
- de la mozarella
- un peu de gruyère rapé
- du thym

sur les 4 demi tranches:

- des champignos de Paris en lamelles
- du munster
- du cumin

### Et hop au four

J'ai mis tout ça à nouveau au four. Vous surveillez, il faut que le fromage soit bien fondu (pas grillé - après chacun ses goûts)

### Service

Pas de chichi, dans une assiette, servies avec un oeuf au plat par tête (j'aurais bien fait des oeufs pochés, mais je ne les réussis pas à chaque fois, et ce soir là, je sentais que ma marge d'erreur n'existait pas 🥶)

## Bilan

Un triomphe 🎉 (et je peux dorénavant acheter ce que je veux comme pain 😛)

J'ai depuis procédé à diverses variantes (mais toujours ail et huile d'olive):

- champignons + courgettes + gruyère + oignons
- courgette + chèvre (Rondelet) + cumin + échalottes
- tomate + chèvre (Rondelet) + jambon cru + échalottes

A chaque fois, grand succès

> dans le cas du fromage de chèvre, j'ai été généreux en fromage, ça permet de faire une seule tartine (mais je garde l'oeuf)

En prévision: brique de chèvre, sardines marinées, anchois, viande hachée, reblochon, ...

> je tenterais bien une bruschetta Hawaïenne... 🍍🥓

Mais voilà, c'est simple, ça permet de terminer les "surplus", ça plaît pour le moment à tout le monde.

## 🖐️ attention

Si vous avez des ados avec des bagues aux dents, ils vont en perdre quelques unes. J'ai donc fait une adaptation "pain de mie", mais attention à la 2ème partie de cuisson, ça grille beaucoup plus vite que le pain aux graines, donc garnissez complèteme pour que le pain de mie ne soit plus visible, ça le protègera. Mais bien sûr il faudra le toaster avant avec les autres tartines, le frotter à l'ail et lui "donner" de l'huile d'olive.

Bon appétit (et je prends toutes les bonnes idées **simples**, parce que si le confinement dure jusqu'en Septembre, je ne tiendrais pas uniquement avec mes bruschettas 😉)

> on récapitule pour les ingrédients:
>
> - un bon gros pain aux graines
> - une gousse d'ail
> - de l'huile d'olive
> - oeufs au plat
> - tout ce qui vous tombe sous la main

<disqus/>

<last-articles/>
