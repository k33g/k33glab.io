---
title: 🇫🇷 Type Result en Kotlin
lang: fr-FR
date: "2020-10-31"
month: "2020-10"
classification: "Kotlin"
teaser: ""
---

# 🇫🇷 Type Result en Kotlin: Update

Suite à une remarque de **[Martial Maillot](https://twitter.com/maillotm)** (puis de [@davinkevin](https://twitter.com/davinkevin)), il y a une méthode du type `Result` bien plus appropriée et élégante: `[runCatching](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/run-catching.html)`, ce qui nous donnera ceci:


```kotlin
fun divide(a: Int, b: Int): Result<Int> {
  return Result.runCatching { a/b }
}

divide(42, 0)
  .onFailure { failure -> println("😡 ${failure.message}") }
  .onSuccess { result -> println("🙂 result=${result}") }
```

## Débats

### Exceptions

Ensuite il y a un débat à propos des exceptions:

[par @CedricChampeau](https://twitter.com/CedricChampeau/status/1322492828451295235)

> Mon gros souci avec cette approche est que tu balances une exception pour la capturer immédiatement après: c'est une horreur en termes de flow ET de performance.

[par @yannick_loiseau](https://twitter.com/yannick_loiseau)

> Ca c'est du au mélange de paradigmes. Soit tu fais des exceptions tout le long, soit des types algebriques tout le long aussi. Quand c'est hybride c'est pas idéal mais c'est normal. Regarde le ! de Rust p.ex., c'est magnifique (bcp plus que ce que fait Go ama, sauf defer)

[par @maillotm](https://twitter.com/maillotm)

> Tout a fait, je préfère utiliser un Either perso. Par contre quand tu appelles du code java, c'est super utile !


### Either

[par @dimiclarence](https://twitter.com/dimiclarence/status/1322517380552163329)

> Puisque tu utilises Arrow autant utiliser le type Either<Left, Right> qui est plus approprié pour ce genre de cas,  avec Left qui contient l'erreur et Right avec la value.


Alors, justement je n'utilise plus **Arrow**; ça me fait une dépendance en moins, surtout que j'en avais besoin pour uniquement pour un ou deux type, et avec le `Result` natif ça serait dommage de s'en priver. Mais je comprend la problématique d'optimisation au niveau de la gestion des exception (mon problème est juste "esthétique", je trouve que `Failure` c'est plus parlant que `Left`)

### Exceptions bis

Autre remarque intéréssante par [par @maillotm](https://twitter.com/maillotm/status/1322515163145244673)

> Par contre le combo runCatching / Result est nickel pour appeler des APIs de lib java qui peuvent potentiellement remonter une exception.


### A lire

Très intéressant, ce document, notamment pour comprendre les remarques précédentes: [Encapsulate successful or failed function execution](https://github.com/Kotlin/KEEP/blob/master/proposals/stdlib/result.md#encapsulate-successful-or-failed-function-execution)


Continuez à gérer vos erreurs 😉

<disqus/>

<last-articles/>

