# Kubernetes

> WIP :construction:

## 🇫🇷 Kit de survie K8S pour les dévs avec K3S

> - [Partie 1: Intro et création du cluster](https://k33g.gitlab.io/articles/2020-02-21-K3S-01-CLUSTER.html)
> - [Partie 2: Le déploiement](https://k33g.gitlab.io/articles/2020-02-21-K3S-02-FIRST-DEPLOY.html)
> - [Partie 2bis: Le déploiement: quelques améliorations](https://k33g.gitlab.io/articles/2020-02-21-K3S-03-FIRST-DEPLOY-FIX.html)
> - [Partie 3: Automatiser le déploiement](https://k33g.gitlab.io/articles/2020-02-23-K3S-04-BETTER-DEPLOY.html)
> - [Partie 4: Utiliser une registry privée (unsecure)](https://k33g.gitlab.io/articles/2020-02-27-K3S-05-REGISTRY.html)
> - [Partie 5: Les Volumes](https://k33g.gitlab.io/articles/2020-02-29-K3S-06-VOLUMES.html)
> - [Partie 6: Redis](https://k33g.gitlab.io/articles/2020-02-29-K3S-07-REDIS.html)
> - [Partie 7: Vert-x](https://k33g.gitlab.io/articles/2020-03-15-K3S-08-VERT-X.html)
> - [Partie 8: un peu de réseau](https://k33g.gitlab.io/articles/2020-03-21-K3S-09-DNSMASQ.html)
> - [Partie 9: retour à Vagrant](https://k33g.gitlab.io/articles/2020-04-18-K3S-10-VAGRANT.html)

## 🇫🇷 J'utilise K3S comme un Paas

> - [Partie 1 - un Workflow plus simple](https://k33g.gitlab.io/articles/2020-04-05-K3S-AS-A-PAAS.html)
> - [Partie 2 - Builder les images dans K3S avec Kaniko](https://k33g.gitlab.io/articles/2020-04-10-K3S-AS-A-PAAS.html)